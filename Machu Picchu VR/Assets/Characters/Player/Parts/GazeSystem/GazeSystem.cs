﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.AI;

public class GazeSystem : MonoBehaviour {

	private Camera cam;


	//Walk
	public NavMeshAgent agent;
	public GameObject WalkIndicator;
	public float WalkDistance = 10.0f ; 
	public float MinWalkDistance = 1.5f ; 
	public AudioSource FeetAudio;
	public double StepCycle = 0.6;
	private double StepTime = 0;
	public float StepRandom = 0.1f;
	public AudioClip[] StepClips;

	//Interact
	public GameObject InteractIndicator;
	public Image InteractRadial;
	public float interactionTime = 1f;
	public float InteractDistance = 3.0f ; 
	public float MinInteractDistance = 0.3f ;
	private float interactionCount = 0;
	private bool interacting = false;
	private bool interactionDone = false;

	void Start ()
	{
		cam = Camera.main;

		//Walk
		agent.updateRotation = false;
	}

	void FixedUpdate () 
	{

		Ray ray = cam.ViewportPointToRay (new Vector3(0.5f,0.5f,0));   
		RaycastHit hit;

		if (Physics.Raycast (ray, out hit, Mathf.Infinity)) {
			if (hit.transform.gameObject.layer == 9) {WalkHit (hit);} 
			else {WalkNotHit ();}

			if (hit.transform.gameObject.layer == 10) {InteractHit (hit);} 
			else {InteractNotHit ();}
            
		} else {
			WalkNotHit ();
			InteractNotHit ();
		}

	}

	
	//Interact
	void InteractHit(RaycastHit hit){
		if (hit.distance > InteractDistance || hit.distance < MinInteractDistance) {
			InteractNotHit ();
		} else {
			if (interacting) {
				if (!interactionDone) {
					interactionCount += Time.deltaTime;
					InteractRadial.fillAmount = interactionCount / interactionTime;
					if (interactionCount > interactionTime) {
						interactionDone = true;
						InteractRadial.fillAmount = 0f;
						hit.transform.gameObject.SendMessage("Interact");
					}
				}
			} else {
				interacting = true;
				interactionDone = false;
				interactionCount = 0;
				InteractIndicator.SetActive (true);
			}
		}
	}

	void InteractNotHit(){
		interacting = false;
		InteractRadial.fillAmount = 0f;
		InteractIndicator.SetActive (false);
	}


	//Walk
	void WalkHit(RaycastHit hit){
		if (hit.distance > WalkDistance || hit.distance < MinWalkDistance) {
			WalkNotHit ();
		} else {
			agent.SetDestination (hit.point);
			WalkIndicator.SetActive (true);
			StepProgress ();
		}
	}

	void WalkNotHit(){
		agent.ResetPath ();
		WalkIndicator.SetActive (false);
	}

	void StepProgress(){
		StepTime += Time.deltaTime;
		if (StepTime > StepCycle) {
			StepTime = Random.Range(0f,StepRandom);
			// pick & play a random footstep sound from the array,
			// excluding sound at index 0
			int n = Random.Range(1, StepClips.Length);
			FeetAudio.clip = StepClips[n];
			FeetAudio.PlayOneShot(FeetAudio.clip);
			// move picked sound to index 0 so it's not picked next time
			StepClips[n] = StepClips[0];
			StepClips[0] = FeetAudio.clip;
		}
	}
}
