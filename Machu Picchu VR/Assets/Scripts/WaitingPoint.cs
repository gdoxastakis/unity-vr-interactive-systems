﻿using System.Collections;
using UnityEngine;

public class WaitingPoint : MonoBehaviour
{
    [SerializeField]
    float debugDrawRadius = 1.0F;

    void OnDrawGizmos()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(transform.position, debugDrawRadius);
    }
}
