﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FireInteractable : MonoBehaviour
{

    public bool Lit = false;
    public ParticleSystem Fire;
    public AudioSource FireAudio;
    public int Duration= 30;
    Collider m_Collider;

    // Start is called before the first frame update
    void Start()
    {
        m_Collider = GetComponent<Collider>();
        if (Lit)
        {
            m_Collider.enabled = false;
            Fire.Play();
            FireAudio.Play();
            Invoke("FireStop", Duration);
        }
    }

    void Interact()
    {
        if (!Lit)
        {
            //m_Collider.enabled = false;
            Fire.Play();
            FireAudio.Play();
            Lit = true;
            Invoke("FireStop", Duration);
        }
    }

    void FireStop()
    {
        Fire.Stop();
        FireAudio.Stop();
        Lit = false;
        m_Collider.enabled = true;
    }
}
