﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DoorInteractable : MonoBehaviour
{

    private bool Open = false;

    public bool OpenDirectionR = true;
    public Animator DoorAnim;
    public AudioSource DoorAudio;
    public int Duration = 30;
    Collider m_Collider;

    void Start()
    {
        m_Collider = GetComponent<Collider>();
    }

    void Interact()
    {
        if (!Open)
        {
            m_Collider.enabled = false;
            if (OpenDirectionR)
            {
                DoorAnim.SetTrigger("OpenR");
            }
            else
            {
                DoorAnim.SetTrigger("OpenL");
            }
            DoorAudio.Play();
            Open = true;
            Invoke("DoorClose", Duration);
        }
    }

    void DoorClose()
    {
        DoorAnim.SetTrigger("Close");
        DoorAudio.Play();
        Open = false;
        m_Collider.enabled = true;
    }
}
