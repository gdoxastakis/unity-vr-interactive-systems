﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ArtifactInteractable : MonoBehaviour
{
    public GameObject wispObject;
    public GameObject meshObject;

    Collider m_Collider;

    void Start()
    {
        m_Collider = GetComponent<Collider>();
    }

    void Interact()
    {
        m_Collider.enabled = false;
        wispObject.SetActive(true);
        meshObject.SetActive(false);
        
    }
  
    
}
