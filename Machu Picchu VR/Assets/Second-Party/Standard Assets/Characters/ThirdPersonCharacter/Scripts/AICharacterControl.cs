using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

namespace UnityStandardAssets.Characters.ThirdPerson
{
    [RequireComponent(typeof (UnityEngine.AI.NavMeshAgent))]
    [RequireComponent(typeof (ThirdPersonCharacter))]
    public class AICharacterControl : MonoBehaviour
    {
        public UnityEngine.AI.NavMeshAgent agent { get; private set; }             // the navmesh agent required for the path finding
        public ThirdPersonCharacter character { get; private set; } // the character we are controlling
        public List<Transform> targets;
        public float waitTime = 5;
        private Transform target; // target to aim for
        private bool waiting = false;


        private void Start()
        {
            // get the components on the object we need ( should not be null due to require component so no need to check )
            agent = GetComponentInChildren<UnityEngine.AI.NavMeshAgent>();
            character = GetComponent<ThirdPersonCharacter>();

	        agent.updateRotation = false;
	        agent.updatePosition = true;
            ChooseDestination();
        }


        private void Update()
        {
            if (target != null)
                agent.SetDestination(target.position);

            if (agent.remainingDistance > agent.stoppingDistance)
                character.Move(agent.desiredVelocity, false, false);
            else
                character.Move(Vector3.zero, false, false);
                if (!waiting)
                {
                    Invoke("ChooseDestination", waitTime);
                    waiting = true;
                }
        }


        public void SetTarget(Transform target)
        {
            this.target = target;
        }

        private void ChooseDestination()
        {
            if (targets.Count != 0)
            {
                int choice = Random.Range(0, targets.Count);
                this.target = targets[choice];
                waiting = false;
            }
            
        }
    }
}
